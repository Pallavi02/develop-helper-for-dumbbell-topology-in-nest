# SPDX-License-Identifier: GPL-2.0-only
# Copyright (c) 2021-2023 NITK Surathkal

########################
# SHOULD BE RUN AS ROOT
########################
from nest.topology import *
from nest.topology.dumbbell_helper import DumbbellHelper

# This program emulates a Dumbbell topology consisting of two routers 'lr' and 'rr',
# three hosts 'lh1' to 'lh3' connected to router 'lr' and five hosts 'rh1' to 'rh5' 
# connected to router 'rr'. Routers 'lr' and 'rr' are connected via a bottleneck link. 
# Five ping packets are sent from `lh1` to `rh4`, five from `rh2` to `lh3` and lastly,
# the success/failure of these packets are reported.

#################################################################################
#                           Network Topology                                    #
#                                                                               #
#                                                                               #
#                                                                               #
#    lh1------------------     Bottleneck link    ------------------rh1         #
#                         \                      / -----------------rh2         #
#                          \   5mbit, 100ms-->  / /                             #
#    lh2-------------------lr -----------------rr ------------------rh3         #
#                          /   <--5mbit, 90ms   \ \                             #
#                         /                      \  ----------------rh4         #
#    lh3------------------                        -------------------rh5        #
#                                                                               #
#        10mbit, 50ms                                  10mbit, 50ms             #
#                                                                               #
#                                                                               #
#################################################################################

# Create a dummbeel topology with 3 left hosts, 5 right hosts with 
# "192.168.1.0/24" as left network address, "192.168.2.0/24" as bottleneck 
# network address and "192.168.3.0/24" as right network address
d = DumbbellHelper(3,5,"192.168.1.0/24", "192.168.2.0/24", "192.168.3.0/24")

# Set link attributes for all links between left side hosts and left router
d.left_interface_attributes("10mbit", "50ms")

# Set link attributes for all links between right router and right side hosts
d.right_interface_attributes("10mbit", "50ms")

# Set link attributes from left router to right router (lr-->rr)
d.left_router_attributes("5mbit","100ms","codel")

# Set link attributes from right router to left router (rr-->lr)
d.right_router_attributes("5mbit","90ms","pie")

# `Ping` from `lh1` to `rh4`
d.left_host(1).ping(d.right_interface(4).address)

# `Ping` from `rh2` to `lh3`
d.right_host(2).ping(d.left_interface(3).address)