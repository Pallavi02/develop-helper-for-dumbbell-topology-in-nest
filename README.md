# Develop-helper-for-dumbbell-topology-in-NeST

## Course Code: CS751

## Assignment: #3

### Overview

NeST (Network Stack Tester) is a python based package which provides intuitive APIs for emulating networks. Dumbbell topology is widely used for network experiments especially for analysing the TCP congestion control algorithms. The dumbbell topology consists of a number of TCP senders, TCP receivers and two routers (between the senders and receivers) . The number of senders and receivers may vary. Currently if a user has to setup such a topology in NeST then it takes a lot of time and efforts.The main objective of this project is to develop a helper to setup dumbbell topology in NeST and provide easy-to-use APIs for the users.

### References

[1]  https://nest.nitk.ac.in/

[2] https://gitlab.com/nsnam/ns-3-dev/-/blob/master/src/point-to-point-layout/model/point-to-point-dumbbell.cc


