from nest.experiment import *
from nest.topology import *
from nest.topology.network import Network
from nest.routing.routing_helper import RoutingHelper
from nest.topology.address_helper import AddressHelper
"""Dumbbell-helper API"""

#################################################################################
# Topology: Dumbbell helper with user-specified number of hosts on either sides.#
# Left hosts: m, Right hosts: n                                                 #
#                                                                               #
#   lh1----------------                         -----------------rh1            #
#                      \                       /                                #
#   lh2---------------  \                     /   ---------------rh2            #
#                      \ \  Bottleneck link  /  /                               #
#   lh3---------------- lr ------------- ---rr ------------------rh3            #
#   .                  /   		            \                      .            #
#   .                 /                      \                     .            #
#   .                /                        \                    .            #
#   .               /                          \                   .            #
#   lhm------------                              ----------------rhn            #
#                                                                               #
#################################################################################

class DumbbellHelper():
    """Defines dumbbell topology"""

    def __init__(self, left_count, right_count, left_network, bottleneck_network, right_network):

        """
        Parameters
        ----------
        left_hosts : int
            Number of left hosts
        right_hosts : int
            Number of right hosts
        left_network : Network address/str
            Network address for the left side network
        right_network : Network address/str
            Network address for the right side network  
        bottleneck_network : NetworkId/str
            NetworkId for the bottleneck network  
        """

        self.left_count = left_count
        self.right_count = right_count
        self.left_hosts = []
        self.right_hosts = []
        self.left_interfaces = []
        self.right_interfaces = []
        self.router_cnt = 2

        # Creating the routers for the dumbbell topology
        self.left_router = Router("lr") 
        self.right_router = Router("rr")

        # Creating all the left hosts
        for host in range(self.left_count):
            self.left_hosts.append(Node("lh" + str(host + 1)))

        # Creating all the right hosts  
        for host in range(self.right_count): 
            self.right_hosts.append(Node("rh" + str(host + 1)))

        # Setting left_network, right_network, and bottleneck_network
        self.left_network = Network(left_network)
        self.right_network = Network(right_network)
        self.bottleneck_network = Network(bottleneck_network)

        # Connections of the left-hosts to the left-router
        for i in range(self.left_count): 
            self.left_interfaces.append(connect(self.left_hosts[i], self.left_router, "etl" + str(i + 1), "etlr" + str(i + 1), network = self.left_network))

		# Connections of the right-hosts to the right-router 
        for i in range(self.right_count):
            self.right_interfaces.append(connect(self.right_hosts[i], self.right_router, "etr" + str(i + 1), "etrr" + str(i + 1), network = self.right_network))

		# Connecting the two routers
        (self.left_router_interface, self.right_router_interface) = connect(self.left_router, self.right_router, network = self.bottleneck_network)
        
        # Assign addresses to all interfaces
        AddressHelper.assign_addresses()
        
        # Set routes by populating the routing tables using DFS
        RoutingHelper(protocol="static").populate_routing_tables()

    def left_interface(self, i):
        """Getter for ith left-interface"""
        return self.left_interfaces[i - 1][0]

    def right_interface(self, i):
        """Getter for ith right-interface"""
        return self.right_interfaces[i - 1][0]

    def left_host(self, i):
        """Getter for ith left-host"""
        return self.left_hosts[i - 1]

    def right_host(self, i):
        """Getter for ith right-host"""
        return self.right_hosts[i - 1]

    def set_left_hosts(self, hosts_cnt):
        """Setter for left-hosts count"""
        self.left_count = hosts_cnt
    
    def get_left_hosts(self):
        """Getter for left-hosts count"""
        return self.left_count

    def set_right_hosts(self, hosts_cnt):
        """Setter for right-hosts count"""
        self.right_count = hosts_cnt
    
    def get_right_hosts(self):
        """Getter for right-hosts count"""
        return self.right_count 

    def set_left_network(self, left_network_address):
        """Setter for left-network"""
        self.left_network = Network(left_network_address)
    
    def get_left_network(self):
        """Getter for left-network"""
        return self.left_network    

    def set_right_network(self, right_network_address):
        """Setter for right-network"""
        self.right_network = Network(right_network_address)
    
    def get_right_network(self):
        """Getter for right-network"""
        return self.right_network  

    def set_bottleneck_network(self, router_network_address):
        """Setter for bottleneck-network"""
        self.right_network = Network(router_network_address)
    
    def get_bottleneck_network(self):
        """Getter for bottleneck-network"""
        return self.bottleneck_network 

    def left_router(self):
        """Getter for left-router"""
        return self.left_router

    def right_router(self):
        """Getter for right-router"""
        return self.right_router     

    def left_interface_attributes(self, bandwidth:str, delay:str): 
        """
        Add attributes bandwidth and delay to interfaces between 
        the hosts on the left-side and the left-router

        Parameters
        ----------
        bandwidth : str/Bandwidth
            Packet outgoing rate
        delay : str/Delay
            Delay before packet is sent out
        """ 
        for i in range(self.left_count):
            self.left_interfaces[i][0].set_attributes(bandwidth, delay)
            self.left_interfaces[i][1].set_attributes(bandwidth, delay)

    def right_interface_attributes(self, bandwidth:str, delay:str):
        """
        Add attributes bandwidth and delay to interfaces between 
        the hosts on the right-side and the right-router

        Parameters
        ----------
        bandwidth : str/Bandwidth
            Packet outgoing rate
        delay : str/Delay
            Delay before packet is sent out
        """
        for i in range(self.right_count):
            self.right_interfaces[i][0].set_attributes(bandwidth, delay)
            self.right_interfaces[i][1].set_attributes(bandwidth, delay)
 
    def left_router_attributes(self, bandwidth:str, delay:str, qdisc:str = None):
        """
        Add attributes bandwidth, delay and qdisc to interface (left-router ---> right-router)

        Parameters
        ----------
        bandwidth : str/Bandwidth
            Packet outgoing rate
        delay : str/Delay
            Delay before packet is sent out
        qdisc : string
            The Queueing algorithm to be added to interface
            (Default value = None)
        """
        self.left_router_interface.set_attributes(bandwidth, delay, qdisc)

    def right_router_attributes(self, bandwidth:str, delay:str, qdisc:str = None):
        """
        Add attributes bandwidth, delay and qdisc to interface (right-router ---> left-router)

        Parameters
        ----------
        bandwidth : str/Bandwidth
            Packet outgoing rate
        delay : str/Delay
            Delay before packet is sent out
        qdisc : string
            The Queueing algorithm to be added to interface
            (Default value = None)
        """
        self.right_router_interface.set_attributes(bandwidth, delay, qdisc)
 
    def __str__(self):
        return "This is Dumbbell helper class"